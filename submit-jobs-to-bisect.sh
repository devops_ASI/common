#!/bin/bash

jq '.[].build_status' build.json | grep -q fail
if [[ $? -eq 1 ]]; then
  echo "Nothing to bisect."
  exit 1
fi

# LATEST_GOOD is coming from various pipeline varaible section.
if [[ ${LATEST_GOOD} != "" ]]; then
  # shellcheck disable=SC2089
  gitlab_curl_post="-F variables[OLD]=\"${LATEST_GOOD}\""
fi

GITLAB_PROJECT_ID="${GITLAB_PROJECT_ID:-"linaro/lkft/bisect"}"
project_id="${GITLAB_PROJECT_ID//\//%2F}"
number_of_builds=$(grep -c tuxbuild_status build.json)
number_of_builds=$((number_of_builds - 1))
start=0
for i in $(seq $start $number_of_builds); do
  build_status=$(jq --argjson i "$i" -r '.[$i | tonumber]["build_status"]' build.json)
  if [[ "${build_status}" == "fail" ]]; then
    new_build_url=$(jq --argjson i "$i" -r '.[$i | tonumber]["download_url"]' build.json)
    new_build_url=$(echo "${new_build_url}" | sed -re 's|/$||')
    tuxmake_image="$(curl -fsSL "${new_build_url}"/metadata.json | jq -r '.build.reproducer_cmdline' | grep -- --image | sed -e 's|.*tuxmake|tuxmake|' -e 's|".*||')"
    # shellcheck disable=SC2086,SC2090
    curl -f -X POST -F token="${CI_JOB_TOKEN}" -F ref=master -F "variables[NEW_BUILD_URL]=${new_build_url}" -F "variables[TUXMAKE_IMAGE]=${tuxmake_image}" -F "variables[BISECT_COUNT]=${BISECT_COUNT}" ${gitlab_curl_post} "${CI_API_V4_URL}/projects/${project_id}/trigger/pipeline" > curl_output.json 2>&1
    echo "Bisect url: $(jq -r '.web_url' curl_output.json)"
    break
  fi
done
