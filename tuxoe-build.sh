#!/bin/bash

set -ex

OE_DISTRO=${OE_DISTRO:-"lkft"}
OE_IMAGES=${OE_IMAGES:-"rpb-console-image-lkft"}
OE_MACHINE=${OE_MACHINE:-"juno"}
OE_RELEASE=${OE_RELEASE:-"sumo"}
SRCREV=${SRCREV:-"87d888a197db9443026045ba3c253a1a929114d4"}
TUXOE_CONTAINER=${TUXOE_CONTAINER:-"ubuntu-16.04"}

case "${REPO_NAME}" in
  linux-mainline)
    kernel_recipe="linux-generic-mainline"
    kernel_recipe_version="git%"
    ;;
  linux-next)
    kernel_recipe="linux-generic-next"
    kernel_recipe_version="git%"
    ;;
  linux-stable-rc | linux-stable)
    kernel_recipe="linux-generic-stable-rc"
    if [ -v KERNEL_BRANCH ]; then
      major_minor="$(echo "${KERNEL_BRANCH}" | sed -e 's#^linux-##' | cut -d\. -f1,2)"
      kernel_recipe_version="${major_minor}+git%"
    fi
    ;;
esac

cat << EOF > tux.json
{
  "container": "${TUXOE_CONTAINER}",
  "sources": {
    "repo": {
      "branch": "${OE_RELEASE}",
      "manifest": "default.xml",
      "url": "https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest"
    }
  },
  "envsetup": "setup-environment",
  "distro": "${OE_DISTRO}",
  "machine": "${OE_MACHINE}",
  "target": "${OE_IMAGES}",
  "local_conf": [
    "SRCREV_kernel = \"${SRCREV}\"",
    "PREFERRED_PROVIDER_virtual/kernel = \"${kernel_recipe}\"",
    "PREFERRED_VERSION_linux-generic-mainline = \"${kernel_recipe_version}\""
  ],
  "artifacts": [
    "\$DEPLOY_DIR"
  ]
}
EOF

cat tux.json

tuxsuite bake submit tux.json --json-out tuxoe-build.json
download_url=$(jq -r '.download_url' tuxoe-build.json)
if [ -v LATEST_RELEASE_TOOLS_KERNEL ] && [ "${CI_COMMIT_BRANCH}" == "${LATEST_RELEASE_TOOLS_KERNEL}" ]; then
  echo "Downloading artifacts..."
  mkdir -p "images/${OE_MACHINE}"
  cd "images/${OE_MACHINE}" || exit
  curl -sSL -o dirlist.json "${download_url}images/${OE_MACHINE}/?export=json"
  tarxz="$(jq -r '.[][] | select(.Url | endswith("rootfs.tar.xz")) | .Url' dirlist.json)"
  extgz="$(jq -r '.[][] | select(.Url | endswith("rootfs.ext4.gz")) | .Url' dirlist.json)"
  curl -sSL "${tarxz}" -o "rpb-console-image-lkft-${OE_MACHINE}.tar.xz"
  curl -sSL "${extgz}" -o "rpb-console-image-lkft-${OE_MACHINE}.ext4.gz"
  cd - || exit
  echo "TUXOE_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}/artifacts/" | tee build-oe.json
else
  echo "No artifacts downloaded"
  echo "TUXOE_URL=${download_url}" | tee build-oe.json
  rootfs_filename="$(jq -r '.[][] | select(.Url | endswith("rootfs.tar.xz")) | .Url' dirlist.json | awk -F '/' '{print $NF}' | sed 's|rootfs.*|rootfs|g')"
  echo "ROOTFS_FILENAME=${rootfs_filename}" | tee -a build-oe.json
fi
echo "OE_MACHINE=${OE_MACHINE}" | tee -a build-oe.json
